package controllers

import play.api.mvc.{ Action, Controller }
import com.atlassian.connect.playscala.PlayAcConfigured
import com.atlassian.connect.playscala.controllers.ActionJwtValidator
import com.atlassian.connect.playscala.auth.Token
import service.{ RedisHeartbeatService, InMemoryHeartBeatService, ViewerDetailsService }
import play.api.libs.json._
import play.api.libs.Crypto
import com.atlassian.connect.playscala.store.DefaultDbConfiguration

/**
 * @since v1.0
 */
object Poller extends Controller with ViewerDetailsService with ActionJwtValidator with DefaultDbConfiguration with PlayAcConfigured {

  def index() = Action { implicit request =>
    jwtValidated { implicit token: Token =>
      val hostId = token.acHost.key.value
      val maybeUserId = token.user

      (for {
        resourceId <- request.getQueryString("issue_key")
      } yield {
        maybeUserId.fold(Unauthorized(views.html.anonymous(hostId, resourceId, ""))) { (userId: String) =>
          //          InMemoryHeartBeatService.put(hostId, resourceId, userId)
          RedisHeartbeatService.put(hostId, resourceId, userId)
          val viewersWithDetails = getViewersWithDetails(resourceId, hostId)
          Ok(views.html.poller(Json.toJson(viewersWithDetails).toString(), hostId, resourceId, userId))
        }
      }) getOrElse BadRequest("Missing issue key")
    }
  }
}
